# `CNPX Project` — Aplicación de prueba para seguro SOAT (Frontend)

## Comenzar

Descargar el archivo ZIP o clonar con git

### Pre-requisitos

1. Necesitas clonar este proyecto.
2. También necesitas tener instalado node, puedes descargarlo e instalarlo dirigiendote[aquí][node].
3. Además necesitas descargar e instalar mongoDB, puedes conseguirlo justo [aquí][mongo]
3. Y necesitas descargar e instalar Bower, puedes conseguirlo justo [aquí][bower]

### Instalar Dependencias

1. Corre ```bower install```
2. Corre ```npm install```

### Para minificación y concatenación

Cada vez que hagas un cambio a los archivos de la carpeta hotel, o admin, y al archivo app.css debes hacer:

1. correr ```gulp js```
2. correr ```gulp css```
2. correr ```gulp html```

ó ```gulp html js css``` para hacer todo junto.

Puedes modificar el archivo que está en la carpeta raíz llamado `gulpfile.js` para ajustarlo a tu gusto.


### Levantar la aplicación

1. Correr ```npm start``` o ```node server.js``` 
2. ir a [http://localhost:8000][local]


[node]: https://nodejs.org/
[mongo]: https://www.mongodb.com/es
[local]: http://localhost:8000
[bower]: https://bower.io/