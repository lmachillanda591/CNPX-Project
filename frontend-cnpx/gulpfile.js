var gulp = require('gulp'),
  jsmin = require('gulp-jsmin'),
  rename = require('gulp-rename'),
  minifyCSS = require('gulp-minify-css'),
  concatCss = require('gulp-concat-css'),
  htmlmin = require('gulp-htmlmin'),
  uglify = require('gulp-uglify'),
  concat = require('gulp-concat');

gulp.task('js', function () {
  gulp.src('app/home/home.js')
    .pipe(jsmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('app/home/min'));
  gulp.src(['app/home/min/home.min.js'])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('app'));
});

gulp.task('css', function () {
  gulp.src('app/app.css')
    .pipe(concatCss("app.min.css"))
    .pipe(minifyCSS({keepBreaks:false}))
    .pipe(gulp.dest('app/'));
});

gulp.task('html', function() {
  gulp.src('app/home/home.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/home/min'));
  gulp.src('app/home/home-step1.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/home/min'));
  gulp.src('app/home/home-step2.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/home/min'));
  gulp.src('app/home/home-step3.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/home/min'));
  gulp.src('app/home/home-step4.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/home/min'));
  gulp.src('app/home/home-step5.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('app/home/min'));
});