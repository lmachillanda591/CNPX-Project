var port = Number(process.env.PORT || 8000);
var express = require('express'); 
var app = express();
//var io = require('socket.io').listen(app);

app.use(express.static(__dirname + '/app'));
var server = app.listen(port, function() { console.log('Listening on port %d', server.address().port); })