'use strict';
// Declare app level module which depends on views, and components
angular.module('myApp', ['ngRoute', 'myApp.factory', 'myApp.routes', 'ui.router', 'myApp.home'])

.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  //$locationProvider.hashPrefix('!');
  //$routeProvider.otherwise({redirectTo: '/editor'});
}]);
