'use strict';

angular.module('myApp.factory', ['ngRoute'])
.factory('apiFactory', function($http){
  return {
    getUser: function(id_number) {
      return $http.get('http://localhost:3000/api/v1/users/' + id_number, {});
    },
    saveUser: function(params) {
      return $http.post('http://localhost:3000/api/v1/users/', {user: params});
    },
    savePayment: function(params) {
      return $http.post('http://localhost:3000/api/v1/policy_payments/', {policy_payment: params});
    },
    saveSoat: function(params) {
      return $http.post('http://localhost:3000/api/v1/soats/', {soat: params});
    },
    editUser: function(id_number, params) {
      return $http.put('http://localhost:3000/api/v1/users/' + id_number, {user: params});
    },
    getSoat: function(license_plate) {
      return $http.get('http://localhost:3000/api/v1/soats/' + license_plate, {});
    },
    getClass: function(id) {
      return $http.get('http://localhost:3000/api/v1/vehicle_classes/' + id, {});
    },
    getDocTypes: function() {
      return $http.get('http://localhost:3000/api/v1/document_types/', {});
    },
    getVehicleClass: function() {
      return $http.get('http://localhost:3000/api/v1/vehicle_classes/', {});
    }
  };
});