'use strict';

angular.module('myApp.routes', ['ngRoute', 'ui.router'])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise('/home/user');
  $stateProvider
  .state('home', {
    url: '/home',
    abstract: true,
    templateUrl: 'home/min/home.html',
    controller: 'homeCtrl'
  })
  .state('home.step1', {
    url: '/user',
    templateUrl: 'home/min/home-step1.html'
  })
  .state('home.step2', {
    url: '/vehicle',
    templateUrl: 'home/min/home-step2.html'
  })
  .state('home.step3', {
    url: '/data',
    templateUrl: 'home/min/home-step3.html'
  })
  .state('home.step4', {
    url: '/payment',
    templateUrl: 'home/min/home-step4.html'
  })
  .state('home.step5', {
    url: '/finish',
    templateUrl: 'home/min/home-step5.html'
  })
}])