
'use strict';angular.module('myApp.home',['ngRoute']).controller('homeCtrl',['$scope','$rootScope','$http','$timeout','apiFactory','$state',function($scope,$rootScope,$http,$timeout,apiFactory,$state){$state.go("home.step1")
$scope.user={};$scope.car={};$scope.pay={};apiFactory.getDocTypes().then(function(response){$scope.document_types=response.data})
apiFactory.getVehicleClass().then(function(response){$scope.vehicle_classes=response.data;})
$scope.getUser=function(id_number){apiFactory.getUser(id_number).then(function(response){$scope.user=response.data;})}
$scope.saveUser=function(){if($scope.user.id){apiFactory.editUser($scope.user.id,$scope.user).then(function(response){toastr.info('Se ha guardado la información del usuario exitosamente')})}
else{apiFactory.saveUser($scope.user).then(function(response){$scope.user=response.data;toastr.info('Se ha guardado el usuario exitosamente')})}}
$scope.emptyUser=function(){$scope.user={};}
$scope.getLicensePlate=function(license_plate){apiFactory.getSoat(license_plate).then(function(response){$scope.show_vehicle_data=false;toastr.error('Esta vehículo ya tiene seguro SOAT','Estimado Usuario')
$scope.car.license_plate=undefined;},function(error){$scope.show_vehicle_data=true;})}
$scope.verClasses=function(id){var vehicle_class=$scope.vehicle_classes.filter(function(c){return c.id==id;});$scope.car.vehicle_subtype_id=vehicle_class[0].vehicle_subtype_id
$scope.vehicle_class=vehicle_class[0].vehicle_class
$scope.subtype=vehicle_class[0].vehicle_subtype.subtype;$scope.amounts=vehicle_class[0].vehicle_amounts;}
$scope.getAmountData=function(){if($scope.car.vehicle_class_id==1){if($scope.car.vehicle_subtype_number<100){$scope.amount=$scope.amounts[0];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_subtype_number>=100&&$scope.car.vehicle_subtype_number<=200){$scope.amount=$scope.amounts[1];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_subtype_number>200){$scope.amount=$scope.amounts[2];$scope.car.vehicle_amount_id=$scope.amount.id}
else{$scope.amount=$scope.amounts[3];$scope.car.vehicle_amount_id=$scope.amount.id}}
else if($scope.car.vehicle_class_id==2){if($scope.car.vehicle_subtype_number<1500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[0];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[1];$scope.car.vehicle_amount_id=$scope.amount.id}}
else if($scope.car.vehicle_subtype_number>=1500&&$scope.car.vehicle_subtype_number<=2500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[2];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[3];$scope.car.vehicle_amount_id=$scope.amount.id}}
else if($scope.car.vehicle_subtype_number>2500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[4];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[5];$scope.car.vehicle_amount_id=$scope.amount.id}}}
else if($scope.car.vehicle_class_id==3){if($scope.car.vehicle_subtype_number<5){$scope.amount=$scope.amounts[0];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_subtype_number>=5&&$scope.car.vehicle_subtype_number<=15){$scope.amount=$scope.amounts[1];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_subtype_number>15){$scope.amount=$scope.amounts[2];$scope.car.vehicle_amount_id=$scope.amount.id}}
else if($scope.car.vehicle_class_id==4){if($scope.car.vehicle_subtype_number<1500){$scope.amount=$scope.amounts[0];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_subtype_number>=1500&&$scope.car.vehicle_subtype_number<=2500){$scope.amount=$scope.amounts[1];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_subtype_number>2500){$scope.amount=$scope.amounts[2];$scope.car.vehicle_amount_id=$scope.amount.id}}
else if($scope.car.vehicle_class_id==5){if($scope.car.vehicle_subtype_number<1500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[0];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[1];$scope.car.vehicle_amount_id=$scope.amount.id}}
else if($scope.car.vehicle_subtype_number>=1500&&$scope.car.vehicle_subtype_number<=2500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[2];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[3];$scope.car.vehicle_amount_id=$scope.amount.id}}
else if($scope.car.vehicle_subtype_number>2500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[4];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[5];$scope.car.vehicle_amount_id=$scope.amount.id}}}
else if($scope.car.vehicle_class_id==6){if($scope.car.vehicle_subtype_number<2500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[0];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[1];$scope.car.vehicle_amount_id=$scope.amount.id}}
else if($scope.car.vehicle_subtype_number>=2500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[2];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[3];$scope.car.vehicle_amount_id=$scope.amount.id}}}
else if($scope.car.vehicle_class_id==7){if($scope.car.vehicle_subtype_number<1500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[0];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[1];$scope.car.vehicle_amount_id=$scope.amount.id}}
else if($scope.car.vehicle_subtype_number>=1500&&$scope.car.vehicle_subtype_number<=2500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[2];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[3];$scope.car.vehicle_amount_id=$scope.amount.id}}
else if($scope.car.vehicle_subtype_number>2500){if($scope.car.vehicle_age<10){$scope.amount=$scope.amounts[4];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_age>=10){$scope.amount=$scope.amounts[5];$scope.car.vehicle_amount_id=$scope.amount.id}}}
else if($scope.car.vehicle_class_id==8){$scope.amount=$scope.amounts[0];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_class_id==9){if($scope.car.vehicle_subtype_number<10){$scope.amount=$scope.amounts[0];$scope.car.vehicle_amount_id=$scope.amount.id}
else if($scope.car.vehicle_subtype_number>=10){$scope.amount=$scope.amounts[1];$scope.car.vehicle_amount_id=$scope.amount.id}}}
$scope.savePayment=function(){$scope.pay.user_id=$scope.user.id;apiFactory.savePayment($scope.pay).then(function(response){$scope.car.policy_payment_id=response.data.id
apiFactory.saveSoat($scope.car).then(function(response){$state.go("home.step5")
apiFactory.getSoat(response.data.license_plate).then(function(response){$scope.soat_data=response.data})})})}
$scope.downloadPdf=function(){var dd={content:[{text:'Cliente: '+$scope.soat_data.user.name+" "+$scope.soat_data.user.last_name+"\n\n",style:'header'},"Clase del vehículo: "+$scope.soat_data.vehicle_class,"Subtipo del vehículo: "+$scope.soat_data.vehicle_amount.subtype_range?$scope.soat_data.vehicle_amount.subtype_range:"-","Edad del vehículo: "+$scope.soat_data.vehicle_amount.age_range?$scope.soat_data.vehicle_amount.age_range:"-","Tasa comercial: "+$scope.soat_data.vehicle_amount.commercial_rate+"%","Valor prima: "+$scope.soat_data.vehicle_amount.premium_amount+"$","Contribución 50% FOSYGA: "+$scope.soat_data.vehicle_amount.fosyga_contribution+"$","Subtotal de prima: "+$scope.soat_data.vehicle_amount.premium_subtotal+"$","Tasa RUNT: "+$scope.soat_data.vehicle_amount.runt_rate+"%",{text:"TOTAL: "+$scope.soat_data.vehicle_amount.total+"$",style:'total'}],styles:{header:{fontSize:18,bold:true},total:{fontSize:12,bold:true}}}
pdfMake.createPdf(dd).download();}}]);