class User < ApplicationRecord
  belongs_to :document_type
  has_many :policy_payments
end
