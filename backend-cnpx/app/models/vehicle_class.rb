class VehicleClass < ApplicationRecord
	has_many :soats
	has_many :vehicle_amounts
	belongs_to :vehicle_subtype
end