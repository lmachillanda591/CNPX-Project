class Soat < ApplicationRecord
  belongs_to :vehicle_class
  belongs_to :vehicle_subtype
  belongs_to :vehicle_amount
  belongs_to :policy_payment
  validates :license_plate, presence: true, uniqueness: true
end