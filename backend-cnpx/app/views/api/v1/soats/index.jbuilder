json.array! @soats do |s|
	json.merge!	s.attributes
	json.vehicle_class s.vehicle_class.vehicle_class
	json.vehicle_subtype s.vehicle_subtype.subtype
	json.vehicle_amount s.vehicle_amount
	json.policy_payment s.policy_payment
end