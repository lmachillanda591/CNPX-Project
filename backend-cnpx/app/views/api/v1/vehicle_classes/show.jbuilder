json.merge!	@veh_class.attributes
json.vehicle_subtype @veh_class.vehicle_subtype
json.vehicle_amounts do
	json.array! @veh_class.vehicle_amounts
end