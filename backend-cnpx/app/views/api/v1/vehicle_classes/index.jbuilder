json.array! @veh_class do |vc|
	json.merge!	vc.attributes
	json.vehicle_subtype vc.vehicle_subtype
	json.vehicle_amounts do
		json.array! vc.vehicle_amounts
	end
end