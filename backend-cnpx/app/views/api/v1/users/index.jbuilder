json.array! @users do |user|
	json.merge! user.attributes
	json.document_type user.document_type
end