class Api::V1::UsersController < ApplicationController
  
  def index
    @users = User.all
    msj= "No hay usuarios registrados"
    if @users.present?
      render 'index'
    else
      render :json => msj.to_json, :status => 404
    end
  end

  def show
    @user = User.find_by_document_number(params[:id])
    if @user.present?
      render 'show'
    else
      msj= "El usuario no existe"
      render :json => msj.to_json, :status => 404
    end
  end

  def create
    user = User.new(user_params)
    if user.save
      render json: user , status: 201
    else
      render json: { errors: user.errors}, status: 422
    end
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      render 'show', status: 200
    else
      render json: { errors: @user.errors }, status: 422
    end
  end

  private

  def user_params
    fields = [
      :document_type_id,
      :document_number,
      :name,
      :last_name,
      :email,
      :phone
    ]
    params.require(:user).permit(fields)
  end
end