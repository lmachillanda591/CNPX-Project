class Api::V1::VehicleClassesController < ApplicationController
  
  def index
    @veh_class = VehicleClass.all
    msj= "No hay clases de vehiculos registradas"
    if @veh_class.present?
      render 'index'
    else
      render :json => msj.to_json, :status => 404
    end
  end

  def show
    @veh_class = VehicleClass.find_by_id(params[:id])
    if @veh_class.present?
      render 'show'
    else
      msj= "La clase del vehiculo no existe"
      render :json => msj.to_json, :status => 404
    end
  end

  def create
    ActiveRecord::Base.transaction do
      veh_class = VehicleClass.new(vehicle_class_params)
      if veh_class.save
        render json: veh_class , status: 201
      else
        render json: { errors: veh_class.errors}, status: 422
      end
    end
  end

  def update
    @veh_class = VehicleClass.find(params[:id])
    if @veh_class.update(vehicle_class_params)
      render 'show', status: 200
    else
      render json: { errors: @veh_class.errors }, status: 422
    end
  end

  private

  def vehicle_class_params
    fields = [
      :vehicle_class
    ]
    params.require(:vehicle_class).permit(fields)
  end
  
end