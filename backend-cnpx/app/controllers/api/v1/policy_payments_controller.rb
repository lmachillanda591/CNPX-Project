class Api::V1::PolicyPaymentsController < ApplicationController
  def index
    @payments = PolicyPayment.all
    msj= "No hay pagos registrados"
    if @payments.present?
      render 'index'
    else
      render :json => msj.to_json, :status => 404
    end
  end

  def show
    @payment = PolicyPayment.find_by_id(params[:id])
    if @payment.present?
      render 'show'
    else
      msj= "El pago no existe"
      render :json => msj.to_json, :status => 404
    end
  end

  def create
    ActiveRecord::Base.transaction do
      payment = PolicyPayment.new(policy_payment_params)
      if payment.save
        render json: payment, status: 201
      else
        render json: { errors: payment.errors}, status: 422
      end
    end
  end

  def update
    @payment = payment.find(params[:id])
    if @payment.update(policy_payment_params)
      render 'show', status: 200
    else
      render json: { errors: @payment.errors }, status: 422
    end
  end

  private

  def policy_payment_params
    fields = [
      :user_id,
      :credit_card_number,
      :cardholder,
      :cvv_code,
      :expiration_date_month,
      :expiration_date_year,
      :installments
    ]
    params.require(:policy_payment).permit(fields)
  end
end