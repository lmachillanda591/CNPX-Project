class Api::V1::VehicleSubtypesController < ApplicationController
  
  def index
    @veh_subtypes = VehicleSubtype.all
    msj = "No hay clases de vehiculos registradas"
    if @veh_subtypes.present?
      render 'index'
    else
      render :json => msj.to_json, :status => 404
    end
  end

  def show
    @veh_subtype = VehicleSubtype.find_by_id(params[:id])
    if @veh_subtype.present?
      render 'show'
    else
      msj = "La clase del vehiculo no existe"
      render :json => msj.to_json, :status => 404
    end
  end

  def create
    ActiveRecord::Base.transaction do
      veh_subtypes = VehicleSubtype.new(vehicle_subtype_params)
      if veh_subtypes.save
        render json: veh_subtypes , status: 201
      else
        render json: { errors: veh_subtypes.errors}, status: 422
      end
    end
  end

  def update
    @veh_subtypes = VehicleSubtype.find(params[:id])
    if @veh_subtype.update(vehicle_subtype_params)
      render 'show', status: 200
    else
      render json: { errors: @allergic.errors }, status: 422
    end
  end

  private

  def vehicle_subtype_params
    fields = [
      :subtype
    ]
    params.require(:vehicle_subtype).permit(fields)
  end
  
end