class Api::V1::SoatsController < ApplicationController
  
  def index
    @soats = Soat.all
    msj= "No hay seguros soat registrados"
    if @soats.present?
      render 'index'
    else
      render :json => msj.to_json, :status => 404
    end
  end

  def show
    @soat = Soat.find_by_license_plate(params[:id])
    if @soat.present?
      render 'show'
    else
      msj= "El seguro soat no existe"
      render :json => msj.to_json, :status => 404
    end
  end

  def create
    ActiveRecord::Base.transaction do
      soat = Soat.new(soat_params)
      if soat.save
        render json: soat, status: 201
      else
        render json: { errors: soat.errors}, status: 422
      end
    end
  end

  private

  def soat_params
    fields = [
      :vehicle_class_id,
      :vehicle_subtype_id,
      :vehicle_amount_id,
      :license_plate,
      :vehicle_age,
      :passenger_number,
      :vehicle_cylinder,
      :vehicle_tons,
      :policy_payment_id
    ]
    params.require(:soat).permit(fields)
  end
end