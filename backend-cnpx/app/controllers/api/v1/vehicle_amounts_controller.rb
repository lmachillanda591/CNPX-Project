class Api::V1::VehicleAmountsController < ApplicationController

  def index
    @veh_amounts = VehicleAmount.all
    msj = "No hay montos de vehiculos con esta clase registrados"
    if @veh_amounts.present?
      render 'index'
    else
      render :json => msj.to_json, :status => 404
    end
  end

  def show
    @veh_amount = VehicleAmount.find(params[:id])
    if @veh_amount.present?
      render 'show'
    else
      msj = "El monto del vehiculo no existe"
      render :json => msj.to_json, :status => 404
    end
  end

  def create
    ActiveRecord::Base.transaction do
      veh_amount = VehicleAmount.new(vehicle_class_params)
      if veh_amount.save
        render json: veh_amount , status: 201
      else
        render json: { errors: veh_amount.errors}, status: 422
      end
    end
  end

  def update
    @veh_amount = VehicleAmount.find(params[:id])
    if @veh_amount.update(vehicle_class_params)
      render 'show', status: 200
    else
      render json: { errors: @allergic.errors }, status: 422
    end
  end

  private

  def vehicle_class_params
    fields = [
      :commercial_rate,
      :premium_amount,
      :fosyga_contribution,
      :premium_subtotal,
      :runt_rate,
      :total,
      :vehicle_class_id,
      :vehicle_subtype_id
    ]
    params.require(:vehicle_subtype).permit(fields)
  end
  
end