class Api::V1::DocumentTypesController < ApplicationController
  def index
    @dt = DocumentType.all
    msj= "No hay tipo de documentos soat registrados"
    if @dt.present?
      render :json => @dt.to_json
    else
      render :json => msj.to_json, :status => 404
    end
  end

  def show
    @dt = DocumentType.find_by_document_type(params[:id])
    if @dt.present?
      render :json => @dt.to_json
    else
      msj= "El tipo de documento no existe"
      render :json => msj.to_json, :status => 404
    end
  end

  private

  def document_type_params
    fields = [
      :document_type
    ]
    params.require(:document_type).permit(fields)
  end
end