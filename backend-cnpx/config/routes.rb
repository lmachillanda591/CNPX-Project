Rails.application.routes.draw do
	namespace :api, defaults: { format: :json } do
		namespace :v1 do
			resources :vehicle_classes
			resources :vehicle_subtypes
			resources :vehicle_class_vechicle_subtypes
			resources :vehicle_amounts
			resources :document_types
			resources :users
			resources :policy_payments
			resources :soats
		end
	end
end