# `CNPX Project` — Aplicación de prueba para seguro SOAT (Backend)

## Comenzar

Descargar el archivo ZIP o clonar con git

### Pre-requisitos

1. Necesitas clonar este proyecto.
2. También necesitas tener instalado rails, postgresql.

### Instalar Gemas
1. Corre ```bundle install```

### Habilitar base de datos
1. Corre ```rake db:setup```
2. Corre ```rake db:seeds``` para las semillas.

### Levantar la aplicación

1. Correr ```rails s``` 
2. ir a [http://localhost:3000][local]

[local]: http://localhost:3000


### API

## USER (USUARIO)

#Buscar un usario por número de documento:

[GET]http://localhost:3000/api/v1/users/123456789

	Response:

	{"id":1,
	"document_type_id":1,
	"document_number":"123456789",
	"name":"Luis",
	"last_name":"Machillanda",
	"email":"holavale@gmail.com",
	"phone":"123128132",
	"created_at":"2017-05-24T00:26:33.566Z",
	"updated_at":"2017-05-24T00:26:33.566Z",
	"document_type":{
		"id":1,
		"document_type":"Cédula de Ciudadanía",
		"created_at":"2017-05-24T00:26:33.535Z",
		"updated_at":"2017-05-24T00:26:33.535Z"}}

#Buscar un todos los usuarios:

[GET]http://localhost:3000/api/v1/users

	Response:

	[
		{
			"id":1,
			"document_type_id":1,
			"document_number":"123456789",
			"name":"Luis",
			"last_name":"Machillanda",
			"email":"holavale@gmail.com",
			"phone":"123128132",
			"created_at":"2017-05-24T00:26:33.566Z",
			"updated_at":"2017-05-24T00:26:33.566Z",
			"document_type":
				{
					"id":1,
					"document_type":"Cédula de Ciudadanía",
					"created_at":"2017-05-24T00:26:33.535Z",
					"updated_at":"2017-05-24T00:26:33.535Z"
				}
		}
	]


#Crear un usuario

[POST]'http://localhost:3000/api/v1/users/', {user: params}

	Body:

	{	document_type_id: integer, 
		document_number: string, 
		name: string, 
		last_name: string, 
		email: string, 
		phone: string
	}

#Updatear un usuario

[PUT] 'http://localhost:3000/api/v1/users/:id', {user: params})

	Body:

	{	document_type_id: integer, 
		document_number: string, 
		name: string, 
		last_name: string, 
		email: string, 
		phone: string
	}

## DOCUMENTS TYPE (TIPO DE DOCUMENTOS)

#Buscar todos los tipos de documentos:

[GET] 'http://localhost:3000/api/v1/document_types'

	Response:

	[
		{
			"id":1,
			"document_type":"Cédula de Ciudadanía",
			"created_at":"2017-05-24T00:26:33.535Z",
			"updated_at":"2017-05-24T00:26:33.535Z"},
		{
			"id":2,
			"document_type":"Cédula de Extranjería",
			"created_at":"2017-05-24T00:26:33.538Z",
			"updated_at":"2017-05-24T00:26:33.538Z"},
		{
			"id":3,
			"document_type":"Registro Civil",
			"created_at":"2017-05-24T00:26:33.539Z",
			"updated_at":"2017-05-24T00:26:33.539Z"},
		{
			"id":4,
			"document_type":"Tarjeta de identidad",
			"created_at":"2017-05-24T00:26:33.541Z",
			"updated_at":"2017-05-24T00:26:33.541Z"}
	]


## VEHICLE CLASSES (CLASES DE VEHÍCULOS)

#Buscar todos las clases de los vehículos:

[GET] 'http://localhost:3000/api/v1/vehicle_classes'
	
	Response:

	[
		{
			"id":1,
			"vehicle_class":"Motos",
			"vehicle_subtype_id":2,
			"created_at":"2017-05-24T00:26:33.291Z",
			"updated_at":"2017-05-24T00:26:33.291Z",
			"vehicle_subtype":{
				"id":2,
				"subtype":"CC",
				"created_at":"2017-05-24T00:26:33.220Z",
				"updated_at":"2017-05-24T00:26:33.220Z"},
				"vehicle_amounts":
					[
						{
							"id":1,
							"commercial_rate":8.26,
							"premium_amount":203100,
							"vehicle_class_id":1,
							"fosyga_contribution":101550,
							"premium_subtotal":304650,
							"runt_rate":1610,
							"total":306260,
							"subtype_range":"Menor a 100 cc",
							"age_range":null,
							"created_at":"2017-05-24T00:26:33.351Z",
							"updated_at":"2017-05-24T00:26:33.351Z"
						}
					]
			}
		]

	#Buscar una clases de los vehículos por ID:

	[GET] 'http://localhost:3000/api/v1/vehicle_classes/1'

		Response:

		{
			"id":1,
			"vehicle_class":"Motos",
			"vehicle_subtype_id":2,
			"created_at":"2017-05-24T00:26:33.291Z",
			"updated_at":"2017-05-24T00:26:33.291Z",
			"vehicle_subtype":{
				"id":2,
				"subtype":"CC",
				"created_at":"2017-05-24T00:26:33.220Z",
				"updated_at":"2017-05-24T00:26:33.220Z"},
				"vehicle_amounts":
					[
						{
							"id":1,
							"commercial_rate":8.26,
							"premium_amount":203100,
							"vehicle_class_id":1,
							"fosyga_contribution":101550,
							"premium_subtotal":304650,
							"runt_rate":1610,
							"total":306260,
							"subtype_range":"Menor a 100 cc",
							"age_range":null,
							"created_at":"2017-05-24T00:26:33.351Z",
							"updated_at":"2017-05-24T00:26:33.351Z"
						}
					]
			}



## VEHICLE SUBTYPES (SUBTIPOS DE VEHÍCULOS)

#Buscar todos los subtipos de los vehículos:

[GET] 'http://localhost:3000/api/v1/vehicle_subtypes'
	
	Response:

	[
		{
			"id":1,
			"subtype":"Ninguno",
			"created_at":"2017-05-24T00:26:33.211Z",
			"updated_at":"2017-05-24T00:26:33.211Z",
			"vehicle_class":
				{
					"id":8,
					"vehicle_class":"Buses y busetas de servicio público urbano",
					"vehicle_subtype_id":1,
					"created_at":"2017-05-24T00:26:33.316Z",
					"updated_at":"2017-05-24T00:26:33.316Z"
				}
		}
	]


#Buscar un los subtipo de vehículo por ID:

[GET] 'http://localhost:3000/api/v1/vehicle_subtypes/1'
	
	Response:

	{
		"id":1,
		"subtype":"Ninguno",
		"created_at":"2017-05-24T00:26:33.211Z",
		"updated_at":"2017-05-24T00:26:33.211Z"
	}


## VEHICLE AMOUNTS (MONTOS DE SEGURO DE LOS VEHÍCULOS)

#Buscar todos los monto:

[GET] 'http://localhost:3000/api/v1/vehicle_amounts'	
	
	Response:

	[
		{
			"id":1,
			"commercial_rate":8.26,
			"premium_amount":203100,
			"vehicle_class_id":1,
			"fosyga_contribution":101550,
			"premium_subtotal":304650,
			"runt_rate":1610,
			"total":306260,
			"subtype_range":"Menor a 100 cc",
			"age_range":null,
			"created_at":"2017-05-24T00:26:33.351Z",
			"updated_at":"2017-05-24T00:26:33.351Z",
			"vehicle_class":
				{
					"id":1,
					"vehicle_class":"Motos",
					"vehicle_subtype_id":2,
					"created_at":"2017-05-24T00:26:33.291Z",
					"updated_at":"2017-05-24T00:26:33.291Z"
				}
		} 
	]

	#Buscar un monto por ID:

	[GET] 'http://localhost:3000/api/v1/vehicle_amounts/1'	
	
		Response:

		{
			"id":1,
			"commercial_rate":8.26,
			"premium_amount":203100,
			"vehicle_class_id":1,
			"fosyga_contribution":101550,
			"premium_subtotal":304650,
			"runt_rate":1610,
			"total":306260,
			"subtype_range":"Menor a 100 cc",
			"age_range":null,
			"created_at":"2017-05-24T00:26:33.351Z",
			"updated_at":"2017-05-24T00:26:33.351Z",
			"vehicle_class":
				{
					"id":1,
					"vehicle_class":"Motos",
					"vehicle_subtype_id":2,
					"created_at":"2017-05-24T00:26:33.291Z",
					"updated_at":"2017-05-24T00:26:33.291Z"
				}
		}


## POLICY PAYMENTS (PAGO DE POLIZAS DE LOS VEHICULOS)

#Buscar todos los pagos de polizas:

[GET] 'http://localhost:3000/api/v1/policy_payments'
	
	Response:

	[
		{
			"id":1,
			"user_id":1,
			"credit_card_number":"123456789",
			"cardholder":"Luis Machillanda",
			"cvv_code":"123",
			"expiration_date_month":23,
			"expiration_date_year":23,
			"installments":2,
			"created_at":"2017-05-24T00:26:33.598Z",
			"updated_at":"2017-05-24T00:26:33.598Z",
			"user":{
				"id":1,
				"document_type_id":1,
				"document_number":"123456789",
				"name":"Luis",
				"last_name":"Machillanda",
				"email":"holavale@gmail.com",
				"phone":"123128132",
				"created_at":"2017-05-24T00:26:33.566Z",
				"updated_at":"2017-05-24T00:26:33.566Z"
			}
		}
	]

#Buscar un pago de poliza por ID:

[GET] 'http://localhost:3000/api/v1/policy_payments/1'
	
	Response:

	{
		"id":1,
		"user_id":1,
		"credit_card_number":"123456789",
		"cardholder":"Luis Machillanda",
		"cvv_code":"123",
		"expiration_date_month":23,
		"expiration_date_year":23,
		"installments":2,
		"created_at":"2017-05-24T00:26:33.598Z",
		"updated_at":"2017-05-24T00:26:33.598Z",
		"user":{
			"id":1,
			"document_type_id":1,
			"document_number":"123456789",
			"name":"Luis",
			"last_name":"Machillanda",
			"email":"holavale@gmail.com",
			"phone":"123128132",
			"created_at":"2017-05-24T00:26:33.566Z",
			"updated_at":"2017-05-24T00:26:33.566Z"
		}
	}
	

#Crear un pago de poliza:

[POST] 'http://localhost:3000/api/v1/policy_payments/, {policy_payment: params}'
	
	Body:

	{
		user_id: integer, 
		credit_card_number: string, 
		cardholder: string, 
		cvv_code: string, 
		expiration_date_month: integer, 
		expiration_date_year: integer, 
		installments: integer
	}

#Updatear un pago de poliza:

[POST] 'http://localhost:3000/api/v1/policy_payments/:id, {policy_payment: params}'
	
	Body:

	{
		user_id: integer, 
		credit_card_number: string, 
		cardholder: string, 
		cvv_code: string, 
		expiration_date_month: integer, 
		expiration_date_year: integer, 
		installments: integer
	}


## SOAT (REGISTRO FINAL DEL SEGURO SOAT)

#Buscar todos los registros SOAT:

[GET] 'http://localhost:3000/api/v1/soats'
	
	Response:

	[
		{
			"id":1,
			"vehicle_class_id":1,
			"vehicle_subtype_id":2,
			"vehicle_amount_id":1,
			"policy_payment_id":1,
			"license_plate":"A12341",
			"vehicle_age":10,
			"vehicle_subtype_number":100,
			"created_at":"2017-05-24T00:26:33.652Z",
			"updated_at":"2017-05-24T00:26:33.652Z",
			"vehicle_class":"Motos",
			"vehicle_subtype":"CC",
			"vehicle_amount":
				{
					"id":1,
					"commercial_rate":8.26,
					"premium_amount":203100,
					"vehicle_class_id":1,
					"fosyga_contribution":101550,
					"premium_subtotal":304650,
					"runt_rate":1610,
					"total":306260,
					"subtype_range":"Menor a 100 cc",
					"age_range":null,
					"created_at":"2017-05-24T00:26:33.351Z",
					"updated_at":"2017-05-24T00:26:33.351Z"
				},
			"policy_payment":
				{
					"id":1,
					"user_id":1,
					"credit_card_number":"123456789",
					"cardholder":"Luis Machillanda",
					"cvv_code":"123",
					"expiration_date_month":23,
					"expiration_date_year":23,
					"installments":2,
					"created_at":"2017-05-24T00:26:33.598Z",
					"updated_at":"2017-05-24T00:26:33.598Z"
				}
		}
	]

#Buscar un los registros SOAT por license_plate (placa del vehiculo):

[GET] 'http://localhost:3000/api/v1/soats'
	
	Response:

	{
		"id":1,
		"vehicle_class_id":1,
		"vehicle_subtype_id":2,
		"vehicle_amount_id":1,
		"policy_payment_id":1,
		"license_plate":"A12341",
		"vehicle_age":10,
		"vehicle_subtype_number":100,
		"created_at":"2017-05-24T00:26:33.652Z",
		"updated_at":"2017-05-24T00:26:33.652Z",
		"vehicle_class":"Motos",
		"vehicle_subtype":"CC",
		"vehicle_amount":
			{
				"id":1,
				"commercial_rate":8.26,
				"premium_amount":203100,
				"vehicle_class_id":1,
				"fosyga_contribution":101550,
				"premium_subtotal":304650,
				"runt_rate":1610,
				"total":306260,
				"subtype_range":"Menor a 100 cc",
				"age_range":null,
				"created_at":"2017-05-24T00:26:33.351Z",
				"updated_at":"2017-05-24T00:26:33.351Z"
			},
		"policy_payment":
			{
				"id":1,
				"user_id":1,
				"credit_card_number":"123456789",
				"cardholder":"Luis Machillanda",
				"cvv_code":"123",
				"expiration_date_month":23,
				"expiration_date_year":23,
				"installments":2,
				"created_at":"2017-05-24T00:26:33.598Z",
				"updated_at":"2017-05-24T00:26:33.598Z"
			}
	}


#Crear un registro SOAT:

[POST] 'http://localhost:3000/api/v1/soats', {soat: params}
	
	Body:

	{
		vehicle_class_id: integer, 
		vehicle_subtype_id: integer, 
		vehicle_amount_id: integer, 
		policy_payment_id: integer, 
		license_plate: string, 
		vehicle_age: integer, 
		vehicle_subtype_number: integer
	}