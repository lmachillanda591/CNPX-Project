class CreateVehicleAmount < ActiveRecord::Migration[5.0]
  def change
    create_table :vehicle_amounts do |t|
      t.float   :commercial_rate
      t.integer :premium_amount
      t.integer :fosyga_contribution
      t.integer :premium_subtotal
      t.integer :runt_rate
      t.integer :total
      t.string  :subtype_range
      t.string  :age_range

      t.timestamps
    end
  end
end
