class CreateSoats < ActiveRecord::Migration[5.0]
  def change
    create_table :soats do |t|
      t.references :vehicle_class, foreign_key: true
      t.references :vehicle_subtype, foreign_key: true
      t.references :vehicle_amount, foreign_key: true
      t.references :policy_payment, foreign_key: true
      t.string :license_plate
      t.integer :vehicle_age
      t.integer :vehicle_tons

      t.timestamps
    end
  end
end
