class CreatePolicyPayments < ActiveRecord::Migration[5.0]
  def change
    create_table :policy_payments do |t|
      t.references :user, foreign_key: true
      t.string :credit_card_number
      t.string :cardholder
      t.string :cvv_code
      t.integer :expiration_date_month
      t.integer :expiration_date_year
      t.integer :installments

      t.timestamps
    end
  end
end
