class CreateVehicleSubtypes < ActiveRecord::Migration[5.0]
  def change
    create_table :vehicle_subtypes do |t|
      t.string :subtype

      t.timestamps
    end
  end
end
