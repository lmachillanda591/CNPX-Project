class CreateVehicleClasses < ActiveRecord::Migration[5.0]
  def change
    create_table :vehicle_classes do |t|
      t.string :vehicle_class
      t.references :vehicle_subtype, foreign_key: true

      t.timestamps
    end
  end
end
