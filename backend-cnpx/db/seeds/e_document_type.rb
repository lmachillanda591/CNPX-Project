ActiveRecord::Base.transaction do
	DocumentType.create!([
		{
			document_type: 'Cédula de Ciudadanía' 
		},
		{
			document_type: 'Cédula de Extranjería'
		},
		{
			document_type: 'Registro Civil'
		},
		{
			document_type: 'Tarjeta de identidad'
		}
	])
end