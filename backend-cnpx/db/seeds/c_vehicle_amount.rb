ActiveRecord::Base.transaction do
	VehicleAmount.create!([
		{
      commercial_rate: 8.26,
      premium_amount: 203100,
      fosyga_contribution: 101550,
      premium_subtotal: 304650,
      runt_rate: 1610,
      total: 306260,
      vehicle_class_id: 1,
      subtype_range: "Menor a 100 cc"
		},
		{
      commercial_rate: 11.09,
      premium_amount: 272700,
      fosyga_contribution: 136350,
      premium_subtotal: 409050,
      runt_rate: 1610,
      total: 410660,
      vehicle_class_id: 1,
      subtype_range: "Entre 100 y 200 cc"
		},
		{
      commercial_rate: 12.51,
      premium_amount: 307600,
      fosyga_contribution: 153800,
      premium_subtotal: 461400,
      runt_rate: 1610,
      total: 463010,
      vehicle_class_id: 1,
      subtype_range: "Menor a 200 cc"
		},
		{
      commercial_rate: 12.51,
      premium_amount: 307600,
      fosyga_contribution: 153800,
      premium_subtotal: 461400,
      runt_rate: 1610,
      total: 463010,
      vehicle_class_id: 1,
      subtype_range: "MOTOCARROS"
		}, 
		# -------------------------- End moto
		{
      commercial_rate: 13.29,
      premium_amount: 326800,
      fosyga_contribution: 163400,
      premium_subtotal: 490200,
      runt_rate: 1610,
      total: 491810,
      vehicle_class_id: 2,
      subtype_range: "Menor a 1500 cc",
      age_range: "entre 0 y 9 años" 
		},
		{
      commercial_rate: 15.99,
      premium_amount: 393200,
      fosyga_contribution: 196600,
      premium_subtotal: 589800,
      runt_rate: 1610,
      total: 591410,
      vehicle_class_id: 2,
      subtype_range: "Menor a 1500 cc",
      age_range: "Mayor o igual a 10 años"
		},
		{
      commercial_rate: 15.88,
      premium_amount: 390400,
      fosyga_contribution: 195200,
      premium_subtotal: 585600,
      runt_rate: 1610,
      total: 587210,
      vehicle_class_id: 2,
      subtype_range: "Entre 1500 y 2500 cc",
      age_range: "Entre 0 y 9 años"
		},
		{
      commercial_rate: 18.82,
      premium_amount: 462700,
      fosyga_contribution: 231350,
      premium_subtotal: 694050,
      runt_rate: 1610,
      total: 695660,
      vehicle_class_id: 2,
      subtype_range: "Entre 1500 y 2500 cc",
      age_range: "Menor o igual a 10 años"
		},
		{
      commercial_rate: 18.63,
      premium_amount: 458100,
      fosyga_contribution: 229050,
      premium_subtotal: 687150,
      runt_rate: 1610,
      total: 688760,
      vehicle_class_id: 2,
      subtype_range: "Mayor a 2500 cc",
      age_range: "Entre 0 y 9 años"
		},
		{
      commercial_rate: 21.39,
      premium_amount: 525900,
      fosyga_contribution: 262950,
      premium_subtotal: 788850,
      runt_rate: 1610,
      total: 790460,
      vehicle_class_id: 2,
      subtype_range: "Mayor a 2500 cc",
      age_range: "Menor o igual a 10 años"
		}, #------------ End camperos y camionetas
		{
      commercial_rate: 14.90,
      premium_amount: 366300,
      fosyga_contribution: 183150,
      premium_subtotal: 549450,
      runt_rate: 1610,
      total: 551060,
      vehicle_class_id: 3,
      subtype_range: "Menor a 5 toneladas"
		},
		{
      commercial_rate: 21.53,
      premium_amount: 529400,
      fosyga_contribution: 264700,
      premium_subtotal: 794100,
      runt_rate: 1610,
      total: 795710,
      vehicle_class_id: 3,
      subtype_range: "Entre 5 y 15 toneladas"
		},
		{
      commercial_rate: 27.23,
      premium_amount: 669600,
      fosyga_contribution: 334800,
      premium_subtotal: 1004400,
      runt_rate: 1610,
      total: 1006010,
      vehicle_class_id: 3,
      subtype_range: "Mayor a 15 toneladas"
		}, #------------- End carga o mixto
		{
      commercial_rate: 16.77,
      premium_amount: 412300,
      fosyga_contribution: 206150,
      premium_subtotal: 618450,
      runt_rate: 1610,
      total: 620060,
      vehicle_class_id: 4,
      subtype_range: "Menor a 1500 cc"
		},
		{
      commercial_rate: 21.15,
      premium_amount: 520000,
      fosyga_contribution: 260000,
      premium_subtotal: 780000,
      runt_rate: 1610,
      total: 781610,
      vehicle_class_id: 4,
      subtype_range: "Entre 1500 y 2500 cc"
		},
		{
      commercial_rate: 25.36,
      premium_amount: 623600,
      fosyga_contribution: 311800,
      premium_subtotal: 935400,
      runt_rate: 1610,
      total: 937010,
      vehicle_class_id: 4,
      subtype_range: "Mayor a 2500 cc"
		}, #----------------end oficiales especiales
		{
      commercial_rate: 7.48,
      premium_amount: 183900,
      fosyga_contribution: 91950,
      premium_subtotal: 275850,
      runt_rate: 1610,
      total: 277460,
      vehicle_class_id: 5,
      subtype_range: "Menor a 1500 cc",
      age_range: "Entre 0 y 9 años"
		},
		{
      commercial_rate: 9.93,
      premium_amount: 244100,
      fosyga_contribution: 122050,
      premium_subtotal: 366150,
      runt_rate: 1610,
      total: 367760,
      vehicle_class_id: 5,
      subtype_range: "Menor a 1500 cc",
      age_range: "Menor o igual a 10 años"
		},
		{
      commercial_rate: 9.12,
      premium_amount: 224200,
      fosyga_contribution: 112100,
      premium_subtotal: 336300,
      runt_rate: 1610,
      total: 337910,
      vehicle_class_id: 5,
      subtype_range: "Entre 1500 y 2500 cc",
      age_range: "Entre 0 y 9 años"
		},
		{
      commercial_rate: 11.35,
      premium_amount: 279100,
      fosyga_contribution: 139550,
      premium_subtotal: 418650,
      runt_rate: 1610,
      total: 420260,
      vehicle_class_id: 5,
      subtype_range: "Entre 1500 y 2500 cc",
      age_range: "Menor o igual a 10 años"
		},
		{
      commercial_rate: 10.66,
      premium_amount: 262100,
      fosyga_contribution: 131050,
      premium_subtotal: 393150,
      runt_rate: 1610,
      total: 394760,
      vehicle_class_id: 5,
      subtype_range: "Mayor a 2500 cc",
      age_range: "Entre 0 y 9 años"
		},
		{
      commercial_rate: 12.65,
      premium_amount: 311000,
      fosyga_contribution: 155500,
      premium_subtotal: 466500,
      runt_rate: 1610,
      total: 468110,
      vehicle_class_id: 5,
      subtype_range: "Mayor a 2500 cc",
      age_range: "Menor o igual a 10 años"
		}, #------------------ end autos familiares
		{
      commercial_rate: 13.37,
      premium_amount: 328700,
      fosyga_contribution: 164350,
      premium_subtotal: 493050,
      runt_rate: 1610,
      total: 494660,
      vehicle_class_id: 6,
      subtype_range: "Menor a 2500 cc",
      age_range: "Entre 0 y 9 años"
		},
		{
      commercial_rate: 17.08,
      premium_amount: 420000,
      fosyga_contribution: 210000,
      premium_subtotal: 630000,
      runt_rate: 1610,
      total: 631610,
      vehicle_class_id: 6,
      subtype_range: "Menor a 2500 cc",
      age_range: "Menor o igual a 10 años"
		},
		{
      commercial_rate: 17.91,
      premium_amount: 440400,
      fosyga_contribution: 220200,
      premium_subtotal: 660600,
      runt_rate: 1610,
      total: 662210,
      vehicle_class_id: 6,
      subtype_range: "Mayor o igual a 2500 cc",
      age_range: "Entre 0 y 9 años"
		},
		{
      commercial_rate: 21.51,
      premium_amount: 528900,
      fosyga_contribution: 264450,
      premium_subtotal: 793350,
      runt_rate: 1610,
      total: 794960,
      vehicle_class_id: 6,
      subtype_range: "Mayor o igual a 2500 cc",
      age_range: "Menor o igual a 10 años"
		}, #------------ end vehiculo para seis o mas pasajeros 
		{
      commercial_rate: 9.28,
      premium_amount: 228200,
      fosyga_contribution: 114100,
      premium_subtotal: 342300,
      runt_rate: 1610,
      total: 343910,
      vehicle_class_id: 7,
      subtype_range: "Menor a 1500 cc",
      age_range: "Entre 0 y 9 años"
		},
		{
      commercial_rate: 11.60,
      premium_amount: 285200,
      fosyga_contribution: 142600,
      premium_subtotal: 427800,
      runt_rate: 1610,
      total: 429410,
      vehicle_class_id: 7,
      subtype_range: "Menor a 1500 cc",
      age_range: "Menor o igual a 10 años"
		},
		{
      commercial_rate: 11.54,
      premium_amount: 283700,
      fosyga_contribution: 141850,
      premium_subtotal: 425550,
      runt_rate: 1610,
      total: 427160,
      vehicle_class_id: 7,
      subtype_range: "Entre 1500 y 2500 cc",
      age_range: "Entre 0 y 9 años"
		},
		{
      commercial_rate: 14.27,
      premium_amount: 350900,
      fosyga_contribution: 175450,
      premium_subtotal: 526350,
      runt_rate: 1610,
      total: 527960,
      vehicle_class_id: 7,
      subtype_range: "Entre 1500 y 2500 cc",
      age_range: "Menor o igual a 10 años"
		},
		{
      commercial_rate: 14.90,
      premium_amount: 366300,
      fosyga_contribution: 183150,
      premium_subtotal: 549450,
      runt_rate: 1610,
      total: 551060,
      vehicle_class_id: 7,
      subtype_range: "Mayor a 2500 cc",
      age_range: "Entre 0 y 9 años"
		},
		{
      commercial_rate: 17.49,
      premium_amount: 430000,
      fosyga_contribution: 215000,
      premium_subtotal: 645000,
      runt_rate: 1610,
      total: 646610,
      vehicle_class_id: 7,
      subtype_range: "Mayor a 2500 cc",
      age_range: "Menor o igual a 10 años"
		}, #--------------------------- end autos de negocios y taxi
		{
      commercial_rate: 21.81,
      premium_amount: 536300,
      fosyga_contribution: 268150,
      premium_subtotal: 804450,
      runt_rate: 1610,
      total: 806060,
      vehicle_class_id: 8
		}, #--------------------------- end Busetas 
		{
      commercial_rate: 22.01,
      premium_amount: 541200,
      fosyga_contribution: 270600,
      premium_subtotal: 811800,
      runt_rate: 1610,
      total: 813410,
      vehicle_class_id: 9,
      subtype_range: "Menor a 10 pasajeros"
		},
		{
      commercial_rate: 31.96,
      premium_amount: 785900,
      fosyga_contribution: 392950,
      premium_subtotal: 1178850,
      runt_rate: 1610,
      total: 1180460,
      vehicle_class_id: 9,
      subtype_range: "Menor o igual a 10 pasajeros"
		}
	])
end