ActiveRecord::Base.transaction do
	PolicyPayment.create!([
		{
			user_id: 1,
			credit_card_number: '123456789',
			cvv_code: '123',
			cardholder: 'Luis Machillanda',
			installments: 2,
			expiration_date_month: 23,
			expiration_date_year: 23
		}
	])
end