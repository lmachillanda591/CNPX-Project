ActiveRecord::Base.transaction do
	VehicleClass.create!([
		{
			vehicle_class: 'Motos',
			vehicle_subtype_id: 2
		},
		{
			vehicle_class: 'Camperos y camionetas',
			vehicle_subtype_id: 2
		},
		{
			vehicle_class: 'Carga o mixto',
			vehicle_subtype_id: 3
		},
		{
			vehicle_class: 'Oficiales especiales',
			vehicle_subtype_id: 2
		},
		{
			vehicle_class: 'Autos familiares',
			vehicle_subtype_id: 2
		},
		{
			vehicle_class: 'Vehículos para seis o mas pasajeros',
			vehicle_subtype_id: 2
		},
		{
			vehicle_class: 'Autos de negocios y taxis',
			vehicle_subtype_id: 2
		},
		{
			vehicle_class: 'Buses y busetas de servicio público urbano',
			vehicle_subtype_id: 1
		},
		{
			vehicle_class: 'Sevicio público intermunicipal',
			vehicle_subtype_id: 4
		}
	])
end