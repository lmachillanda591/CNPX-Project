ActiveRecord::Base.transaction do
	VehicleSubtype.create!([
		{
			subtype: 'Ninguno' 
		},
		{
			subtype: 'CC' 
		},
		{
			subtype: 'Toneladas' 
		},
		{
			subtype: 'Pasajeros'
		},
		{
			subtype: 'Motocarros'
		}
	])
end