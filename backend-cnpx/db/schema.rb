# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170521063504) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "document_types", force: :cascade do |t|
    t.string   "document_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "policy_payments", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "credit_card_number"
    t.string   "cardholder"
    t.string   "cvv_code"
    t.integer  "expiration_date_month"
    t.integer  "expiration_date_year"
    t.integer  "installments"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["user_id"], name: "index_policy_payments_on_user_id", using: :btree
  end

  create_table "soats", force: :cascade do |t|
    t.integer  "vehicle_class_id"
    t.integer  "vehicle_subtype_id"
    t.integer  "vehicle_amount_id"
    t.integer  "policy_payment_id"
    t.string   "license_plate"
    t.integer  "vehicle_age"
    t.integer  "vehicle_subtype_number"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.index ["vehicle_amount_id"], name: "index_soats_on_vehicle_amount_id", using: :btree
    t.index ["policy_payment_id"], name: "index_soats_on_policy_payment_id", using: :btree
    t.index ["vehicle_class_id"], name: "index_soats_on_vehicle_class_id", using: :btree
    t.index ["vehicle_subtype_id"], name: "index_soats_on_vehicle_subtype_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.integer  "document_type_id"
    t.string   "document_number"
    t.string   "name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["document_type_id"], name: "index_users_on_document_type_id", using: :btree
  end

  create_table "vehicle_amounts", force: :cascade do |t|
    t.float    "commercial_rate"
    t.integer  "premium_amount"
    t.integer  "vehicle_class_id"
    t.integer  "fosyga_contribution"
    t.integer  "premium_subtotal"
    t.integer  "runt_rate"
    t.integer  "total"
    t.string   "subtype_range"
    t.string   "age_range"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.index ["vehicle_class_id"], name: "index_vehicle_amounts_on_vehicle_class_id", using: :btree
  end

  create_table "vehicle_classes", force: :cascade do |t|
    t.string   "vehicle_class"
    t.integer  "vehicle_subtype_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["vehicle_subtype_id"], name: "index_vehicle_classes_on_vehicle_subtype_id", using: :btree
  end

  create_table "vehicle_subtypes", force: :cascade do |t|
    t.string   "subtype"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "policy_payments", "users"
  add_foreign_key "soats", "vehicle_amounts"
  add_foreign_key "soats", "vehicle_classes"
  add_foreign_key "soats", "vehicle_subtypes"
  add_foreign_key "soats", "policy_payments"
  add_foreign_key "vehicle_amounts", "vehicle_classes"
  add_foreign_key "users", "document_types"
end
